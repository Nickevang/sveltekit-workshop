import { redirect, type Actions } from '@sveltejs/kit';
import type { PageServerLoad } from './$types';
import { registerSchema } from '$lib/schemas/register';
import { message, superValidate } from 'sveltekit-superforms/server';
import { createPool } from '@vercel/postgres';
import { POSTGRES_URL } from '$env/static/private';

export const load = (async () => {
	const form = await superValidate(registerSchema);
	return {
		form
	};
}) satisfies PageServerLoad;

export const actions = {
	default: async ({ request }) => {
		const form = await superValidate(request, registerSchema);
		const db = createPool({ connectionString: POSTGRES_URL });

		try {
			let statusId;
			switch (form.data.status) {
				case 'Student':
					statusId = 1;
					break;
				case 'Software Engineer':
					statusId = 2;
					break;
				case 'Designer':
					statusId = 3;
					break;
				case 'Other':
					statusId = 4;
					break;
				default:
					statusId = 4;
					break;
			}

			const query = `INSERT INTO "Participant" ("Email", "Name", "LastName", "KaggleId", "GitlabUsername", "Status" ${
				form.data.phone ? ', "Phone"' : ''
			}${form.data.about ? ', "Description"' : ''}${
				form.data.dob ? ', "DateOfBirth"' : ''
			}) VALUES ('${form.data.email}', '${form.data.name}', '${form.data.lastName}', '${
				form.data.kaggle
			}', '${form.data.gitlab}', '${statusId}' ${
				form.data.phone ? ",'" + form.data.phone + "'" : ''
			} ${form.data.about ? ",'" + form.data.about + "'" : ''} ${
				form.data.dob ? ",'" + form.data.dob + "'" : ''
			});`;

			// Create participant
			await db.query(query);

			// Add a ParticipantInterest for every checkbox
			const interestCreations: Promise<unknown>[] = [];
			const interestCheckboxes = {
				1: form.data.ai,
				2: form.data.ml,
				3: form.data.frontEnd,
				4: form.data.backEnd,
				5: form.data.fullStack
			};

			for (const interest of Object.entries(interestCheckboxes)) {
				if (interest[1]) {
					interestCreations.push(
						db.query(
							`INSERT INTO "ParticipantInterests" ("ParticipantId", "InterestId") VALUES ('${form.data.email}', ${interest[0]})`
						)
					);
				}
			}

			await Promise.allSettled(interestCreations);
		} catch (error) {
			return message(
				form,
				'There was an error when trying to sign you up. Please try again later.',
				{
					status: 500
				}
			);
		}
		throw redirect(303, '/participants');
	}
} satisfies Actions;
